#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

layout(origin_upper_left) in vec4 gl_FragCoord;

uniform sampler2D ourTexture;

uniform int transparent;
uniform bool wave;

uniform vec3 color_mul;
uniform float runtime;


void main() {
  vec2 newTexCoord = TexCoord;
  if(wave) {
    newTexCoord.x = TexCoord.x + cos(runtime*2+gl_FragCoord.y*0.15) * 0.001;
    newTexCoord.y = TexCoord.y + sin(runtime*10+gl_FragCoord.x*0.25) * 0.001;
  }
  FragColor = texture(ourTexture, newTexCoord) * vec4(color_mul, 1);
  if(FragColor.r == 0 && FragColor.g == 0 && FragColor.b == 0) FragColor = vec4(0.278431373, 0.176470588, 0.235294118, 1);
  if(transparent == 1) {
    int grid_size = 2;
    int pixel_x = int(gl_FragCoord.x);
    int pixel_y = int(gl_FragCoord.y);
    if(pixel_x % grid_size == pixel_y % grid_size) discard;
  }
}
