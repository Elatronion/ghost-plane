#include "dialogue.h"
#include "jamutils.h"
#include "game_manager.h"

void DrawBox() {
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec2 sprite = { 18, 0 };
  hge_vec3 color = { 0.466666667f, 0.356862745f, 0.639215686f };
  hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "color_mul", color);
  for(int y = 0; y < 4; y++)
  for(int x = 1; x < 15; x++) {
    if(y == 0) sprite.y = 0;
    else if(y == 3) sprite.y = 2;
    else sprite.y = 1;
    if (x == 1) sprite.x = 18;
    else if (x == 14) sprite.x = 20;
    else if(y != 1 && y != 2) sprite.x = 19;
    else {
      sprite.x = 0;
      sprite.y = 0;
    }
    hge_vec3 position = { x * scale.x, -(y + + 11) * scale.y, 99 };
    hgeRenderSpriteSheet(
      hgeResourcesQueryShader("sprite_shader"),
      hgeResourcesQueryTexture("onebit"),
      position, scale, 0.0f,
      frame_resolution, sprite);
  }
}

void DrawBoxText(const char* str) {
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec2 sprite = { 35, 18 };
  hge_vec3 color = { 0.466666667f, 0.356862745f, 0.639215686f };
  for(int i = 0; i < fmin(strlen(str), 24); i++) {
    switch(str[i]) {
      case 58: // :
      sprite.x = 45;
      sprite.y = 17;
      break;
      case 46: // .
      sprite.x = 46;
      sprite.y = 17;
      break;
      case 32: // SPACE
      sprite.x = 0;
      sprite.y = 0;
      break;
      default:
      if (str[i] > 57) { // Letters
        sprite.x = 35 + (str[i]-65)%13;
        sprite.y = 18 + (str[i]-65)/13;
      } else { // Numbers
        sprite.x = 35 + (str[i]-48);
        sprite.y = 17;
      }
    }

    hge_vec3 position = { (2 + i%12) * scale.x, (-12 - (int)(i/12)) * scale.y, 100 };
    hgeRenderSpriteSheet(
      hgeResourcesQueryShader("sprite_shader"),
      hgeResourcesQueryTexture("onebit"),
      position, scale, 0.0f,
      frame_resolution, sprite);
  }
}

void System_Dialogue(hge_entity* entity, dialogue* d) {
  if((int)(hgeRuntime()*10)% 25 < 10) return;
  switch(GetState()) {
    case game_win:
      DrawBox();
      DrawBoxText("  YOU  WIN  PRESS  ENTER");
    break;
    case game_over:
    DrawBox();
    DrawBoxText(" GAME  OVER PRESS  ENTER");
    break;
  }
}
