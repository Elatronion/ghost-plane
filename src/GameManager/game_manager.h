#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H
#include <HGE/HGE_Core.h>
#include "pond.h"
#include "map.h"
#include "jamutils.h"

typedef enum {
  main_menu = 0,
  game_paused,
  game_start,
  game_play,
  game_over,
  game_win
} game_state;

void LoadLevel(const char* level_path);

game_map* AddMap();
void AddEnd(v2i position, const char* next_level);
void AddPlayer(v2i position);
void AddSlider(v2i position, v2i direction, hge_vec2 sprite, hge_vec3 color);

void SetState(game_state s);
game_state GetState();

void SetReplay(bool b);
bool GetReplay();

#endif
