#ifndef GAME_POND_H
#define GAME_POND_H
#include <HGE/HGE_Core.h>
#include "jamutils.h"
#include "game_manager.h"

// Pond Flags
#define POND_FLAG_IS_PLAYER 0x00;
#define POND_FLAG_HURTS_PLAYER 0x01;

// Collision Flags
#define POND_COLLISION_FLAG_NONE 0x00
#define POND_COLLISION_FLAG_UP 0x01
#define POND_COLLISION_FLAG_DOWN 0x02
#define POND_COLLISION_FLAG_LEFT 0x04
#define POND_COLLISION_FLAG_RIGHT 0x08

typedef struct {
  v2i position;
  v2i direction;
  uint8_t flags;
  hge_vec2 sprite;
  hge_vec3 color;
  bool ghost;

  uint8_t collision_flags;
  bool update;
} pond;

typedef struct {

} slider;

typedef struct {
  char next_level[255];
} end;

void System_Pond(hge_entity* entity, pond* game_pond);
void System_PondRendering(hge_entity* entity, pond* game_pond);
void System_Slider(hge_entity* entity, pond* game_pond, slider* c_slider);
void System_End(hge_entity* entity, pond* game_pond, end* end);

#endif
