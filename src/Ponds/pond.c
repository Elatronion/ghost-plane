#include "pond.h"
#include "map.h"

void PondCollision(pond* game_pond) {
  // Borders
  if(game_pond->position.x + game_pond->direction.x < 0) {
    game_pond->collision_flags |= POND_COLLISION_FLAG_LEFT;
  }
  if(game_pond->position.x + game_pond->direction.x > MAP_WIDTH-1) {
    game_pond->collision_flags |= POND_COLLISION_FLAG_RIGHT;
  }
  if(game_pond->position.y + game_pond->direction.y < 0) {
    game_pond->collision_flags |= POND_COLLISION_FLAG_UP;
  }
  if(game_pond->position.y + game_pond->direction.y > MAP_HEIGHT-1) {
    game_pond->collision_flags |= POND_COLLISION_FLAG_DOWN;
  }

  // Map
  game_map* requested_map;
  hge_ecs_request map_request = hgeECSRequest(1, "map");
  for(int i = 0; i < map_request.NUM_ENTITIES; i++) {
    hge_entity* map_entity = map_request.entities[i];
    requested_map = map_entity->components[hgeQuery(map_entity, "map")].data;
  }


  if(requested_map->tiles[game_pond->position.x+game_pond->direction.x][game_pond->position.y+game_pond->direction.y] != TILE_FLOOR) {
    if(game_pond->direction.x > 0)
      game_pond->collision_flags |= POND_COLLISION_FLAG_RIGHT;
    else if(game_pond->direction.x < 0)
      game_pond->collision_flags |= POND_COLLISION_FLAG_LEFT;
    if(game_pond->direction.y > 0)
      game_pond->collision_flags |= POND_COLLISION_FLAG_DOWN;
    else if(game_pond->direction.y < 0)
      game_pond->collision_flags |= POND_COLLISION_FLAG_UP;
  }

  // Player
  if(!game_pond->ghost) return;
  pond* player_pond;
  hge_ecs_request player_pond_request = hgeECSRequest(1, "playable");
  for(int i = 0; i < player_pond_request.NUM_ENTITIES; i++) {
    hge_entity* player_entity = player_pond_request.entities[i];
    player_pond = player_entity->components[hgeQuery(player_entity, "pond")].data;
  }

  if(game_pond->collision_flags & POND_COLLISION_FLAG_RIGHT ||
  game_pond->collision_flags & POND_COLLISION_FLAG_LEFT) {
    if(
      (game_pond->position.x == player_pond->position.x &&
      game_pond->position.y + game_pond->direction.y == player_pond->position.y)
      ) {
      SetState(game_over);
    }
  } else
  if(game_pond->collision_flags & POND_COLLISION_FLAG_UP ||
  game_pond->collision_flags & POND_COLLISION_FLAG_DOWN) {
    if(
      (game_pond->position.x + game_pond->direction.x == player_pond->position.x &&
      game_pond->position.y == player_pond->position.y)
      ) {
      SetState(game_over);
    }
  } else
  if((game_pond->collision_flags & POND_COLLISION_FLAG_UP ||
  game_pond->collision_flags & POND_COLLISION_FLAG_DOWN) &&
  (game_pond->collision_flags & POND_COLLISION_FLAG_RIGHT ||
  game_pond->collision_flags & POND_COLLISION_FLAG_LEFT)) {
    if(
      (game_pond->position.x == player_pond->position.x &&
      game_pond->position.y == player_pond->position.y)
      ) {
      SetState(game_over);
    }
  } else {
    if(
      (game_pond->position.x + game_pond->direction.x == player_pond->position.x &&
      game_pond->position.y + game_pond->direction.y == player_pond->position.y)
      ) {
      SetState(game_over);
    }
  }

}

void System_Pond(hge_entity* entity, pond* game_pond) {
  if(!game_pond->update) return;
  game_pond->collision_flags = POND_COLLISION_FLAG_NONE;

  PondCollision(game_pond);

  // Process Collisions
  if(!(game_pond->collision_flags & POND_COLLISION_FLAG_RIGHT) &&
    !(game_pond->collision_flags & POND_COLLISION_FLAG_LEFT)) {
      game_pond->position.x += game_pond->direction.x;
  }
  if(!(game_pond->collision_flags & POND_COLLISION_FLAG_UP) &&
    !(game_pond->collision_flags & POND_COLLISION_FLAG_DOWN)) {
      game_pond->position.y += game_pond->direction.y;
    }

  game_pond->update = false;
}

void System_PondRendering(hge_entity* entity, pond* game_pond) {
  if((GetState() == game_play) && game_pond->ghost && !GetReplay()) return;
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec3 position = { game_pond->position.x * scale.x, -game_pond->position.y * scale.y, 0 };
  hgeShaderSetFloat(hgeResourcesQueryShader("sprite_shader"), "runtime", hgeRuntime());
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "wave", game_pond->ghost);
  hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "color_mul", game_pond->color);
  hgeRenderSpriteSheet(
    hgeResourcesQueryShader("sprite_shader"),
    hgeResourcesQueryTexture("onebit"),
    position, scale, 0.0f,
    frame_resolution, game_pond->sprite);
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "wave", false);
}

void System_Slider(hge_entity* entity, pond* game_pond, slider* slider) {
  if(game_pond->collision_flags & POND_COLLISION_FLAG_RIGHT) {
    //game_pond->position.x--;
    game_pond->direction.x = -game_pond->direction.x;
  }

  if(game_pond->collision_flags & POND_COLLISION_FLAG_LEFT) {
    //game_pond->position.x++;
    game_pond->direction.x = -game_pond->direction.x;
  }

  if(game_pond->collision_flags & POND_COLLISION_FLAG_UP) {
    //game_pond->position.y++;
    game_pond->direction.y = -game_pond->direction.y;
  }
  if(game_pond->collision_flags & POND_COLLISION_FLAG_DOWN) {
    //game_pond->position.y--;
    game_pond->direction.y = -game_pond->direction.y;
  }
  game_pond->collision_flags = POND_COLLISION_FLAG_NONE;
}

void System_End(hge_entity* entity, pond* game_pond, end* end_c) {
  pond* player_pond;
  hge_ecs_request player_pond_request = hgeECSRequest(1, "playable");
  for(int i = 0; i < player_pond_request.NUM_ENTITIES; i++) {
    hge_entity* player_entity = player_pond_request.entities[i];
    player_pond = player_entity->components[hgeQuery(player_entity, "pond")].data;
  }
  if(
    game_pond->position.x == player_pond->position.x &&
    game_pond->position.y == player_pond->position.y &&
    GetState() != game_win) {
    SetState(game_win);
  }

  // End Rendering
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec3 position = { game_pond->position.x * scale.x, -game_pond->position.y * scale.y, 0 };
  hgeShaderSetFloat(hgeResourcesQueryShader("sprite_shader"), "runtime", hgeRuntime());
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "wave", true);
  hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "color_mul", game_pond->color);
  hgeRenderSpriteSheet(
    hgeResourcesQueryShader("sprite_shader"),
    hgeResourcesQueryTexture("onebit"),
    position, scale, 0.0f,
    frame_resolution, game_pond->sprite);
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "wave", false);
}
