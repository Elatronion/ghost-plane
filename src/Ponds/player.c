#include "player.h"

void UpdateAllPonds() {
  hge_ecs_request pond_request = hgeECSRequest(1, "pond");
  for(int i = 0; i < pond_request.NUM_ENTITIES; i++) {
    hge_entity* pond_entity = pond_request.entities[i];
    pond* requested_pond = pond_entity->components[hgeQuery(pond_entity, "pond")].data;
    requested_pond->update = true;
  }
}

int replay_inputs[500];
int num_replay_inputs = 0;
int cur_replay_input = 0;
float replay_timer = 0.0f;
void AddReplayInput(int id) {
  replay_inputs[num_replay_inputs] = id;
  num_replay_inputs++;
}

void ResetReplay() {
  num_replay_inputs = 0;
}

void LoadReplay() {
  ReloadLevel();
  SetReplay(true);
  cur_replay_input = 0;
  /*num_replay_inputs = 0;

  AddReplayInput(4);
  AddReplayInput(4);
  AddReplayInput(0);
  AddReplayInput(3);
  AddReplayInput(3);
  AddReplayInput(1);*/

  const char* line = "";
  if(strcmp(line, "WALK_UP") == 0)           AddReplayInput(0);
  else if(strcmp(line, "WALK_DOWN") == 0)    AddReplayInput(1);
  else if(strcmp(line, "WALK_LEFT") == 0)    AddReplayInput(2);
  else if(strcmp(line, "WALK_RIGHT") == 0)   AddReplayInput(3);
  else if(strcmp(line, "ACCEPT") == 0)       AddReplayInput(4);
}

void PlayerRenderObserveTokensGUI(player* playable) {
  if(GetState() != game_start) return;
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec2 sprite = { 27, 21 };
  hge_vec3 color = { 0.780392157, 0, 0.223529412 };
  hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "color_mul", color);
  for(int i = 0; i < 5; i++) {
    if(playable->observe_tokens > i) sprite.y = 21;
    else sprite.y = 20;
    hge_vec3 position = { i * scale.x, -0 * scale.y, 0 };
    hgeRenderSpriteSheet(
      hgeResourcesQueryShader("sprite_shader"),
      hgeResourcesQueryTexture("onebit"),
      position, scale, 0.0f,
      frame_resolution, sprite);
  }
}

void System_Player(hge_entity* entity, pond* player_pond, player* playable) {
  // DEBUG COMMANDS
  if(hgeInputGetKeyDown(HGE_KEY_F1)) SetState(game_over);
  PlayerRenderObserveTokensGUI(playable);

  // NORMAL PLAYER
  // Fix double ACCEPT/OBSERVE when loading new map (adding new player)
  if(playable->first_loop) {
    playable->first_loop = false;
    return;
  }
  // Input
  bool WALK_UP = false;
  bool WALK_DOWN = false;
  bool WALK_LEFT = false;
  bool WALK_RIGHT = false;
  bool ACCEPT = false;
  switch(GetReplay()) {
    case true:
    replay_timer+=hgeDeltaTime();
    if(replay_timer >= 0.5f) {
      replay_timer = 0.0f;
      if(cur_replay_input >= num_replay_inputs) return; // This should never be true...
      printf("MOVE: %d!\n", replay_inputs[cur_replay_input]);
      switch(replay_inputs[cur_replay_input]) {
        case 0: // WALK_UP
        WALK_UP = true;
        break;
        case 1: // WALK_DOWN
        WALK_DOWN = true;
        break;
        case 2: // WALK_LEFT
        WALK_LEFT = true;
        break;
        case 3: // WALK_RIGHT
        WALK_RIGHT = true;
        break;
        case 4: // ACCEPT
        ACCEPT = true;
        break;
      }
      cur_replay_input++;
    }
    break;
    case false:
    WALK_UP    = hgeInputGetKeyDown(HGE_KEY_UP);
    WALK_DOWN  = hgeInputGetKeyDown(HGE_KEY_DOWN);
    WALK_LEFT  = hgeInputGetKeyDown(HGE_KEY_LEFT);
    WALK_RIGHT = hgeInputGetKeyDown(HGE_KEY_RIGHT);
    ACCEPT     = hgeInputGetKeyDown(HGE_KEY_ENTER);
    if(WALK_UP) AddReplayInput(0);
    if(WALK_DOWN) AddReplayInput(1);
    if(WALK_LEFT) AddReplayInput(2);
    if(WALK_RIGHT) AddReplayInput(3);
    if(ACCEPT) AddReplayInput(4);
  }

  switch(GetState()) {
    case game_over:
      if(ACCEPT) {
        ResetReplay();
        ReloadLevel();
      }
      if(hgeInputGetKeyDown(HGE_KEY_R)) {
        LoadReplay();
      }
    break;
    case game_win:
      if(ACCEPT) {
        ResetReplay();
        end* end_c;
        hge_ecs_request end_c_request = hgeECSRequest(1, "end");
        for(int i = 0; i < end_c_request.NUM_ENTITIES; i++) {
          hge_entity* end_entity = end_c_request.entities[i];
          end_c = end_entity->components[hgeQuery(end_entity, "end")].data;
        }
        LoadLevel(end_c->next_level);
        return;
      }
      if(hgeInputGetKeyDown(HGE_KEY_R)) {
        LoadReplay();
      }
    break;
    case game_start:
    if(ACCEPT && playable->observe_tokens > 0) {
      playable->observe_tokens--;
      UpdateAllPonds();
    }
    case game_play:
      player_pond->direction.x = 0;
      player_pond->direction.y = 0;

      if(WALK_UP) player_pond->direction.y = -1;
      else if(WALK_DOWN) player_pond->direction.y = 1;
      else if(WALK_LEFT) player_pond->direction.x = -1;
      else if(WALK_RIGHT) player_pond->direction.x = 1;

      if(WALK_UP || WALK_DOWN || WALK_LEFT || WALK_RIGHT) {
        if(GetState() == game_start) SetState(game_play);
        UpdateAllPonds();
      }
    break;
  }
}
