#ifndef PLAYER_H
#define PLAYER_H
#include <HGE/HGE_Core.h>
#include "pond.h"

typedef struct {
  int observe_tokens;

  bool first_loop;
} player;

void System_Player(hge_entity* entity, pond* player_pond, player* playable);
void LoadReplay();

#endif
