#include "map.h"

hge_vec2 GetTileSprite(int tile_id) {
  hge_vec2 frame = { 4, 0 };
  switch(tile_id) {
    case TILE_FLOOR:
      frame.x = 0;
      frame.y = 0;
    break;
    default:
      frame.x = tile_id%(SPRITE_SHEET_WIDTH+1);
      frame.y = tile_id/(SPRITE_SHEET_WIDTH+1);
    break;
  }
  return frame;
}

void System_Map(hge_entity* entity, game_map* map) {
  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  hge_vec3 color_mul = { 0.811764706f, 0.776470588f, 0.741176471f };
  for(int y = 0; y < MAP_HEIGHT; y++)
  for(int x = 0; x < MAP_WIDTH; x++) {
    hge_vec3 position = { x * scale.x, -y * scale.y, -50 };
    hge_vec2 frame = GetTileSprite(map->tiles[x][y]);
    hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "color_mul", color_mul);
    hgeRenderSpriteSheet(
      hgeResourcesQueryShader("sprite_shader"),
      hgeResourcesQueryTexture("onebit"),
      position, scale, 0.0f,
      frame_resolution, frame);
  }
}
