#include "camera_fx.h"
#include "map.h"
#include "jamutils.h"

void System_CameraFX(hge_entity* entity, hge_vec3* position, camera_fx* camfx) {
  hgeClearColor(0.278431373, 0.176470588, 0.235294118, 1);
  hgeClear(0x00004000); // GL_COLOR_BUFFER_BIT
  float shake_x = 10.f;
  camfx->shake_intensity-= hgeDeltaTime();
  camfx->shake_intensity += -camfx->shake_intensity * 5 * hgeDeltaTime();
  camfx->shake_vector.x = RandomFloat(-camfx->shake_intensity, camfx->shake_intensity);
  camfx->shake_vector.y = RandomFloat(-camfx->shake_intensity, camfx->shake_intensity);
  camfx->desired_position.x += (camfx->shake_vector.x - camfx->desired_position.x) * shake_x * hgeDeltaTime();
  camfx->desired_position.y += (camfx->shake_vector.y - camfx->desired_position.y) * shake_x * hgeDeltaTime();
  position->x = (int)(camfx->desired_position.x) + (MAP_WIDTH-1)/2.f * 16;
  position->y = (int)(camfx->desired_position.y) + (-MAP_HEIGHT+1)/2.f * 16;
}
